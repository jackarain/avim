//
// im_handler.cpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2013 Jack (jack.wgm at gmail dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#include "im_handler.hpp"
#include "mime_types.hpp"
#include "reply.hpp"
#include "request.hpp"

namespace http {
namespace avim_server {

im_handler::im_handler(void)
{
}


im_handler::~im_handler(void)
{
}

bool im_handler::operator()(const request& req, reply& rep)
{
	// 暂无实现.
	return false;
}

} // namespace avim_server
} // namespace http
