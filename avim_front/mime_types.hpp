//
// mime_types.hpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#ifndef HTTP_AVIM_SERVER_MIME_TYPES_HPP
#define HTTP_AVIM_SERVER_MIME_TYPES_HPP

#include <string>

namespace http {
namespace avim_server {
namespace mime_types {

/// Convert a file extension into a MIME type.
std::string extension_to_type(const std::string& extension);

} // namespace mime_types
} // namespace avim_server
} // namespace http

#endif // HTTP_AVIM_SERVER_MIME_TYPES_HPP
