//
// file_handler.hpp
// ~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#ifndef HTTP_AVIM_SERVER_FILE_HANDLER_HPP
#define HTTP_AVIM_SERVER_FILE_HANDLER_HPP

#include <string>

namespace http {
namespace avim_server {

struct reply;
struct request;

/// The common handler for all incoming requests.
class file_handler
{
public:
	/// Construct with a directory containing files to be served.
	explicit file_handler(const std::string& doc_root);

	/// Handle a request and produce a reply.
	bool operator()(const request& req, reply& rep);

private:
	/// The directory containing the files to be served.
	std::string doc_root_;

	/// Perform URL-decoding on a string. Returns false if the encoding was
	/// invalid.
	static bool url_decode(const std::string& in, std::string& out);
};

} // namespace avim_server
} // namespace http

#endif // HTTP_AVIM_SERVER_FILE_HANDLER_HPP
