//
// request.hpp
// ~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#ifndef HTTP_AVIM_SERVER_REQUEST_HPP
#define HTTP_AVIM_SERVER_REQUEST_HPP

#include <string>
#include <vector>
#include "header.hpp"

namespace http {
namespace avim_server {

/// A request received from a client.
struct request
{
	/// The request method, e.g. "GET", "POST".
	std::string method;

	/// The requested URI, such as a path to a file.
	std::string uri;

	/// Major version number, usually 1.
	int http_version_major;

	/// Minor version number, usually 0 or 1.
	int http_version_minor;

	/// The headers included with the request.
	std::vector<header> headers;

	/// The optional content sent with the request.
	std::string content;
};

} // namespace avim_server
} // namespace http

#endif // HTTP_AVIM_SERVER_REQUEST_HPP
