//
// im_handler.hpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2013 Jack (jack.wgm at gmail dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#ifndef HTTP_AVIM_SERVER_IM_HANDLER_HPP
#define HTTP_AVIM_SERVER_IM_HANDLER_HPP

namespace http {
namespace avim_server {

struct reply;
struct request;

/// im_handler用于处理所有im通信服务.
class im_handler
{
public:
	/// Construct with a directory containing files to be served.
	explicit im_handler(void);
	~im_handler(void);

	/// Handle a request and produce a reply.
	bool operator()(const request& req, reply& rep);

};

} // namespace avim_server
} // namespace http

#endif // HTTP_AVIM_SERVER_IM_HANDLER_HPP
