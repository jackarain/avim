//
// mime_types.cpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#include "mime_types.hpp"

namespace http {
namespace avim_server {
namespace mime_types {

struct mapping
{
	const char* extension;
	const char* mime_type;
} mappings[] =
{
	{ "gif", "image/gif" },
	{ "htm", "text/html" },
	{ "html", "text/html" },
	{ "jpg", "image/jpeg" },
	{ "png", "image/png" },
	{ 0, 0 } // Marks end of list.
};

std::string extension_to_type(const std::string& extension)
{
	for (mapping* m = mappings; m->extension; ++m)
	{
		if (m->extension == extension)
		{
			return m->mime_type;
		}
	}

	return "text/plain";
}

} // namespace mime_types
} // namespace avim_server
} // namespace http
