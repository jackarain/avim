//
// header.hpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the GNU Affero General Public License, Version 3.0. (See accompanying
// file LICENSE or copy at http://www.gnu.org/licenses/agpl-3.0.txt)
//

#ifndef HTTP_AVIM_SERVER_HEADER_HPP
#define HTTP_AVIM_SERVER_HEADER_HPP

#include <string>

namespace http {
namespace avim_server {

struct header
{
	std::string name;
	std::string value;
};

} // namespace avim_server
} // namespace http

#endif // HTTP_AVIM_SERVER_HEADER_HPP
